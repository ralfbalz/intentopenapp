package ch.volag.intentopenapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OpenActivity extends AppCompatActivity {
    private final String RESULT_STRING = "RESULT_STRING";

    @BindView(R.id.tv_messages)
    TextView mTvMessages;
    @BindView(R.id.ed_result)
    EditText mEdResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        String message = intent.getStringExtra(Intent.EXTRA_TEXT);
        mTvMessages.setText(message);
    }

    @OnClick(R.id.btn_send_result)
    public void sendResult(){

        // Create intent to deliver some kind of result data
        Intent intent = new Intent();
        intent.putExtra(RESULT_STRING, mEdResult.getText().toString());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}